#!/usr/bin/env python3

import json
import os
import sys

f = open('/etc/linuswrapper.json', 'r')
config = json.load(f)
f.close()

if config["QuoteLinus"]:
    print("Hey, you should probably be using " + config["PackageManager"] + " you dunce!")
    print("")
    print("apt and apt-get are only available in Debian based Linux distributions.")
else:
    print("apt and apt-get are only available in Debian based Linux distributions. Use " + config["PackageManager"] + " instead!")

if len(sys.argv) < 2:
    exit(1)

# build pacman command
cmd = sys.argv
cmd[0] = config["PackageManager"]

try:
    cmd[1] = config[cmd[1]]
except:
    exit(1)

cmd_str = ' '.join(cmd)

# prompt user if they want to run the command
print("")
ans = input('Run "' + cmd_str + '"? [Y/n] ')

# run
if ans == '' or ans[0] == 'y' or ans[0] == 'Y':
    os.system(cmd_str)
